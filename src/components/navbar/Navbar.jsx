import React, { useContext } from "react";
import { NavLink, useNavigate, useLocation } from "react-router-dom";
import { HashLink, NavHashLink } from "react-router-hash-link";

import { ButtonComp } from "../button/Button";

import { ReactComponent as Logo } from "../../assets/svg/logo.svg";
import "./navbar.scss";

export const Navbar = () => {
  const navigate = useNavigate();

  // Render the navbar
  return (
    <nav className="navbar" id="top">
      <span className="navbar_container">
        <span className="navbar_logo-menu">
          <span className="logo_container">
            <Logo onClick={() => navigate(`/`)} style={{ cursor: "pointer" }} />
          </span>
          <span className="list_container">
            <ul>
              <li>
                <HashLink smooth to={"/#section-two"}>
                  QSAR
                </HashLink>
              </li>
              <li>
                <HashLink smooth to={"/#section-two"}>
                  Technology
                </HashLink>
              </li>
              <li>
                <HashLink smooth to={"/#section-two"}>
                  Advantages
                </HashLink>
              </li>
              <li>
                <HashLink smooth to={"/#section-two"}>
                  Cooperation
                </HashLink>
              </li>
              <li>
                <HashLink smooth to={"/#section-two"}>
                  Metrics
                </HashLink>
              </li>
              <li>
                <HashLink smooth to={"/#section-two"}>
                  Achievements
                </HashLink>
              </li>
              <li>
                <HashLink smooth to={"/#section-two"}>
                  Articles
                </HashLink>
              </li>
            </ul>
          </span>
        </span>
        <span style={{ height: "100%" }}>
          <ButtonComp
            buttonClassName="btn_antd"
            type="text"
            buttonSize="large"
            onButtonClick={() => console.log("asd")}
          >
            Contact us
          </ButtonComp>
        </span>
      </span>
    </nav>
  );
};
